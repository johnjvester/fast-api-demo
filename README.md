# `fast-api-demo`

> A simple demonstration of a RESTful API microservice using [FastAPI](https://fastapi.tiangolo.com/).

## Publications

This repository is related to the following DZone.com publications:

* [FastAPI Got Me an OpenAPI Spec Really... Fast](https://dzone.com/articles/fastapi-got-me-an-openapi-spec-really-fast)

To read more of my publications or browse my public projects, please review one of the following URLs:

* https://dzone.com/authors/johnjvester
* https://gitlab.com/users/johnjvester/projects

## The Article API

At the heart of this example, the `fast-api-demo` delivers an Article API RESTful microservice, which allows consumers 
to view a list of my recently-published articles on DZone.com. 

A given `Article` contains the following properties:

* `id` – simple unique identifier property (number)
* `title` – the title of the article (string)
* `url` – the full URL to the article (string)
* `year` –  the year the article was published (number)

The Article API will include the following URIs:

* GET `/articles` – will retrieve a list of articles
* GET `/articles/{article_id}` – will retrieve a single article, by the id property
* POST `/articles` – adds a new article

> This service contains a few of my recently published articles programmatically using an in-memory object array.

## Heroku Settings

See the following Heroku documentation for more information:

https://devcenter.heroku.com/articles/getting-started-with-python

## Additional Information

Made with <span style="color:red;">♥</span> &nbsp;by johnjvester@gmail.com, because I enjoy writing code.
